#!/bin/bash

#
#
#    This script is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    This script is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

OLD_DATA=${OLD_DATA:-$(pwd)/tmp/old/}
NEW_DATA=${NEW_DATA:-$(pwd)/tmp/new/}

find_old_bulk() {
	pkg="$1"
	if [ $(grep -R "^$pkg$" $OLD_DATA/veclinux/ | cut -f 1 -d : | wc -l) -gt 1 ]; then
		#echo "WARNING: $pkg is defined more than once." >&2
		mkdir -p $NEW_DATA/veclinux/
		echo "$(grep -R "^$pkg$" $OLD_DATA/veclinux/ )" \
			>> $NEW_DATA/veclinux/DUPLICATED_ENTRIES
		echo "----------------" >> $NEW_DATA/veclinux/DUPLICATED_ENTRIES
		
	fi
	res="$(grep -R "^$pkg$" $OLD_DATA/veclinux/ |cut -f 1 -d :)"
	if [[ "x$res" != "x" ]]; then
		res="$(basename $res)"
		echo "$res"
	fi
	echo ""
}

find_location_on_iso() {
	pkg="$1"
	res="$(grep "/$pkg$" $OLD_DATA/pkgs |cut -f 1 -d /)"
	echo "$res"
}

process_pkgtools_logs() {
	#for p in $(slapt-get --installed | cut -f 1 -d ' '); do
	pushd /var/log/packages
	for p in *; do
		echo "Processing $p"
		pname=`slapt-get --show $p | grep ^"Package Name:" | cut -f 2 -d : | xargs`
		echo "[ Read Package Name ]: $pname"
		#pname=`basename $(/sbin/pkgname /var/log/packages/$p ) || \
		#	/sbin/pkgname $p `

		if [[ "x$pname" = "x" ]]; then
			pname=$(pkgname $p)
		fi
		bulk_loc="$(find_old_bulk $pname)"
		iso_loc="$(find_location_on_iso $pname)"
		mkdir -p $NEW_DATA/veclinux/
		echo "[ Part Of Bulk ]: $bulk_loc"
		echo "[ ISO Location ]: $iso_loc"
#		read
		if [[ "x$bulk_loc" != "x" ]]; then
			echo "$pname" >> $NEW_DATA/veclinux/$bulk_loc
		else
			echo "$pname" >> $NEW_DATA/veclinux/PKGS_TO_LOCATE_IN_BULKS
		fi
		if [[ "x$iso_loc" != "x" ]]; then
			echo "$iso_loc/$pname" >> $NEW_DATA/pkgs
		else
			echo "$pname" >> $NEW_DATA/pkgs_to_locate_in_iso_dirs
		fi
	done
	popd
}

process_pkgtools_logs

