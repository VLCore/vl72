#!/bin/sh
CWD=$(pwd)

printf "Checking irssi executable... "
if [ -x /usr/bin/irssi ]; then
	printf "PASSED\n"
else
	printf "FAILED\n"
	exit 1
fi

NORUN=1 . $CWD/irssi.SlackBuild
rversion="$(irssi -v |cut -f 2 -d ' '|xargs)"

printf "Compare built version to installed version... "
if [[ $VERSION != $rversion ]]; then
	printf "FAILED (Built: ${VERSION} - Installed:  $rversion)\n"
	exit 1
else
	printf "PASSED\n"
fi
exit 0
