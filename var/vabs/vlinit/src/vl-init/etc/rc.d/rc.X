#! /bin/sh
# rc.X   launch display manager if the system is going to level 4/5
#
# Origin : from rc.4 of Slackware 9.0
# Rewritten by Eko M. Budi to adapt vlinit system, 
#
# GNU GPL (c) Eko M. Budi, 2004
#         (c) Vector Linux, 2004

# This setting will be changed by vxdmset
DISPLAY_MANAGER=GDM


#####################################################
# Don't touch below these. Expert is excepted ;)
KDM_CMD=/usr/bin/kdm
GDM_CMD=/usr/bin/gdm
WDM_CMD=/usr/bin/wdm
XDM_CMD=/usr/bin/xdm

# Tell the viewers what's going to happen...
echo
echoc "rc.X ==> Going to multiuser GUI mode ..." yellow
# Get prevlevel
case $PREVLEVEL in
    2|3) PLEVEL=$PREVLEVEL
    ;;
    *)
     # exit and clean the tty1, Thanks to johnb for mentioning it.
    killall -s HUP mingetty
    PLEVEL=2
esac

sleep 1
## Anti quick spawning
## But this does not always work :(
# Check if respawned less then 1 minute, then go init 2
BOOTX=bootx.log
TEST=`find /var/log/ -cmin -1 -name $BOOTX`
if [ "$TEST" ]; then
  echoc "WARNING: Recent X-Window was respawned too short." red
  echo "Please make sure that the X-Window is allright,"
  echo "then try 'init $RUNLEVEL' again after 1 minute."
  echo 
  echoc "Canceling GUI login mode" red
  rm -f $TEST
  /sbin/telinit $PLEVEL
  exit 1
fi
touch /var/log/BOOTX
echo "LAST_X=`date`" > /var/log/$BOOTX

## launch a login manager
for DM in $DISPLAY_MANAGER KDM GDM WDM XDM NONE; do
  case $DM in
  KDM)
    if [ -x $KDM_CMD ]; then
      #echo KDE Display Manager
      exec $KDM_CMD -nodaemon
      exit
    fi
    ;;
  GDM)
    if [ -x $GDM_CMD ]; then
      #echo GNOME Display Manager
      clear
      exec $GDM_CMD -nodaemon
      exit
    fi
    ;; 
  WDM)
    if [ -x $WDM_CMD ]; then
      #echo WING Display Manager
      buildsessions WDM &> /dev/null
      exec $WDM_CMD -nodaemon > /dev/null
      exit
    fi 
    ;;
  XDM)
    if [ -x $XDM_CMD ]; then
      #echo XFree86 Display Manager
      exec $XDM_CMD -nodaemon
      exit
    fi
    ;;
  NONE)
    # error
    echo
    echo "Hey, you don't have KDM, GDM, WDM, or XDM."
    echo "Can't use GUI mode without one of them installed, mate."
    sleep 30
    break
    ;;
  esac
done

echoc "Canceling GUI login mode" red 
/sbin/telinit $PLEVEL

# All done.
